module.exports = {
  content: [],
  theme: {
    extend: {
      colors: {
        primary: '#FFD500',
        secondary: '#005BBB',
        tertiary: '#ff0000'
      }
    },
  },
  plugins: [],
}
