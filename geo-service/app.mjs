import fs from 'fs';
import express from 'express';
import cors from 'cors';

const file = JSON.parse(fs.readFileSync('plz-5stellig-centroid.geojson', { encoding: 'utf-8' }));
const app = express()
const port = 3001

const corsAllowed = ['http://localhost:3000', 'https://beta.jobaidukraine.com']

app.use(cors());

const parseResults = (result) => {
    return result.map(element => {
        return {
            lat: element.geometry.coordinates[0],
            lng: element.geometry.coordinates[1],
            town: element.properties.note.slice(6)
        }
    })
}

app.get('/zip/:zip', (req, res) => {

    if(req.params.zip.length <3) {
        res.json({'error':'Minimum 3 characters are required for parameter.'});
        return;
    }
    let result = file.features.filter(entry => {
        if (entry.properties.plz.indexOf(req.params.zip) > -1) {
            return entry;
        }
    });
    res.json(parseResults(result));
});

app.get('/town/:town', (req, res) => {

    if(req.params.town.length <3) {
        res.json({'error':'Minimum 3 characters are required for parameter.'});
        return;
    }

    const result = file.features.filter(entry => {
        if (entry.properties.note.toLowerCase().indexOf(req.params.town) > -1) {
            return entry;
        }
    });
    res.json(parseResults(result));
});

app.listen(port, () => {
    console.log(`Geo Service listening on port ${port}`)
})



