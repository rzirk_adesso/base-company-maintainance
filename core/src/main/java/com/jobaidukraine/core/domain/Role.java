package com.jobaidukraine.core.domain;

public enum Role {
  ADMIN("ROLE_ADMIN"),
  EDITOR("ROLE_EDITOR"),
  USER("ROLE_USER");

  final String roleName;

  Role(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleName() {
    return roleName;
  }
}
