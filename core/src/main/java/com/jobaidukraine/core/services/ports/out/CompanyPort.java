package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.Company;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CompanyPort {
  Optional<Company> findById(long id);

  Page<Company> findAllByPageable(Pageable pageable);

  Company save(Company company);

  Company update(Company company);

  void delete(long id);
}
