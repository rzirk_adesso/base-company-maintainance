package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserPort {
  Optional<User> findById(long id);

  Page<User> findAllByPageable(Pageable pageable);

  Optional<User> findByEmail(String email);

  User save(User user);

  User update(User user);

  void delete(long id);
}
