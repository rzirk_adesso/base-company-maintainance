package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.services.ports.in.queries.JobQuery;
import com.jobaidukraine.core.services.ports.in.usecases.JobUseCase;
import com.jobaidukraine.core.services.ports.out.JobPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobService implements JobQuery, JobUseCase {

  private final JobPort jobPort;

  @Override
  public Job save(Job job) {
    return this.jobPort.save(job);
  }

  @Override
  public Page<Job> findAllByPageable(Pageable pageable) {
    return this.jobPort.findAllByPageable(pageable);
  }

  @Override
  public Page<Job> findAllByPageable(Pageable pageable, String title) {
    return this.jobPort.findAllByPageable(pageable, title);
  }

  @Override
  public Job findById(long id) {
    return this.jobPort.findById(id).orElseThrow();
  }

  @Override
  public Job update(Job job) {
    return this.jobPort.update(job);
  }

  @Override
  public void delete(long id) {
    jobPort.delete(id);
  }
}
