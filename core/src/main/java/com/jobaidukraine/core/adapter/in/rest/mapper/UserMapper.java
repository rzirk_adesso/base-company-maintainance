package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.UserDto;
import com.jobaidukraine.core.domain.User;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "InRestUserMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {LanguageMapper.class})
public interface UserMapper {
  User dtoToDomain(UserDto userDto);

  UserDto domainToDto(User user);
}
