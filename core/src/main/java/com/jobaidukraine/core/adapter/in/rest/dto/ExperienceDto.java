package com.jobaidukraine.core.adapter.in.rest.dto;

public enum ExperienceDto {
  PROFESSIONAL,
  NEW_GRADUATE,
  STUDENT
}
