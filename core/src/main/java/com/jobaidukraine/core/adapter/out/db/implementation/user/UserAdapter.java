package com.jobaidukraine.core.adapter.out.db.implementation.user;

import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.LanguageMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.UserMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.UserRepository;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.out.UserPort;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserAdapter implements UserPort {

  private final UserRepository userRepository;
  private final UserMapper userMapper;

  private final LanguageMapper languageMapper;

  @Override
  public Optional<User> findById(long id) {
    return userRepository.findById(id).map(userMapper::toDomain);
  }

  @Override
  public Page<User> findAllByPageable(Pageable pageable) {
    return userRepository.findAll(pageable).map(userMapper::toDomain);
  }

  @Override
  public Optional<User> findByEmail(String email) {
    return userRepository.findByEmail(email).map(userMapper::toDomain);
  }

  @Override
  public User save(User user) {
    return userMapper.toDomain(userRepository.save(userMapper.toEntity(user)));
  }

  @Override
  public User update(User user) {
    UserEntity userEntity = userRepository.findById(user.getId()).orElseThrow();

    if (user.getFirstname() != null) {
      userEntity.setFirstname(user.getFirstname());
    }

    if (user.getLastname() != null) {
      userEntity.setLastname(user.getLastname());
    }

    if (user.getEmail() != null) {
      userEntity.setEmail(user.getEmail());
    }

    if (user.getPosition() != null) {
      userEntity.setPosition(user.getPosition());
    }

    if (user.getCountry() != null) {
      userEntity.setCountry(user.getCountry());
    }

    if (user.getLanguageLevels() != null) {
      userEntity.setLanguageLevels(languageMapper.toEntitySet(user.getLanguageLevels()));
    }

    return userMapper.toDomain(userRepository.save(userEntity));
  }

  @Override
  public void delete(long id) {
    UserEntity user = userRepository.findById(id).orElseThrow();
    user.setDeleted(true);
    userRepository.save(user);
  }
}
