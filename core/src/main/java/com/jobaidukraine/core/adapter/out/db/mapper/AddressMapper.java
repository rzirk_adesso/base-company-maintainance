package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.AddressEntity;
import com.jobaidukraine.core.domain.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "OutDbAddressMapperImpl")
public interface AddressMapper {
  Address toDomain(AddressEntity addressEntity);

  AddressEntity toEntity(Address address);
}
