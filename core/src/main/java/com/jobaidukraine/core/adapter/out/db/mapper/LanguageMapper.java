package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.LanguageLevelEntity;
import com.jobaidukraine.core.domain.LanguageLevel;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbLanguageMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface LanguageMapper {
  LanguageLevel toDomain(LanguageLevelEntity languageLevelEntity);

  LanguageLevelEntity toEntity(LanguageLevel languageLevel);

  Set<LanguageLevel> toDomainSet(Set<LanguageLevelEntity> languageEntities);

  Set<LanguageLevelEntity> toEntitySet(Set<LanguageLevel> languageLevels);
}
