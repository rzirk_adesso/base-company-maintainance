package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.HousingEntity;
import com.jobaidukraine.core.domain.Housing;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OurDbHousingMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {AddressMapper.class})
public interface HousingMapper {
  Housing toDomain(HousingEntity housingEntity);

  HousingEntity toEntity(Housing housing);
}
