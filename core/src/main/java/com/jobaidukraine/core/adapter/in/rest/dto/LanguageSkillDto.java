package com.jobaidukraine.core.adapter.in.rest.dto;

public enum LanguageSkillDto {
  NATIVE,
  FLUENT,
  GOOD,
  BASIC
}
