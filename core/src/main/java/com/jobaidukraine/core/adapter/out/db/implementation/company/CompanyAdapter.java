package com.jobaidukraine.core.adapter.out.db.implementation.company;

import com.jobaidukraine.core.adapter.out.db.entities.CompanyEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.CompanyMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.CompanyRepository;
import com.jobaidukraine.core.domain.Company;
import com.jobaidukraine.core.services.ports.out.CompanyPort;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CompanyAdapter implements CompanyPort {

  private final CompanyRepository companyRepository;
  private final CompanyMapper companyMapper;

  @Override
  public Optional<Company> findById(long id) {
    return companyRepository.findById(id).map(companyMapper::toDomain);
  }

  @Override
  public Page<Company> findAllByPageable(Pageable pageable) {
    return companyRepository.findAll(pageable).map(companyMapper::toDomain);
  }

  @Override
  public Company save(Company company) {
    return companyMapper.toDomain(companyRepository.save(companyMapper.toEntity(company)));
  }

  @Override
  public Company update(Company company) {
    CompanyEntity companyEntity = companyRepository.findById(company.getId()).orElseThrow();

    if (company.getName() != null) {
      companyEntity.setName(company.getName());
    }
    if (company.getUrl() != null) {
      companyEntity.setUrl(company.getUrl());
    }
    if (company.getEmail() != null) {
      companyEntity.setEmail(company.getEmail());
    }
    return companyMapper.toDomain(companyRepository.save(companyEntity));
  }

  @Override
  public void delete(long id) {
    CompanyEntity company = companyRepository.findById(id).orElseThrow();
    company.setDeleted(true);
    companyRepository.save(company);
  }
}
