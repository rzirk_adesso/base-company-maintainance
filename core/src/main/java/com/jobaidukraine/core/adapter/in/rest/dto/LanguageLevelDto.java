package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LanguageLevelDto {

  @NotNull(message = "The language may not be null.")
  @NotBlank(message = "The language may not be blank or empty.")
  @Schema(description = "The language.", example = "English")
  private String language;

  @NotNull(message = "The language skill may not be null.")
  @NotBlank(message = "The language skill may not be blank or empty.")
  @Schema(description = "The level of a language.", example = "Native")
  private LanguageSkillDto languageSkill;
}
