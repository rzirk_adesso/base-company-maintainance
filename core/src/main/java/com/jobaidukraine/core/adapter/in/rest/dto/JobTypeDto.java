package com.jobaidukraine.core.adapter.in.rest.dto;

public enum JobTypeDto {
  FREELANCE,
  PERMANENT,
  PART_TIME,
  TEMPORARY
}
