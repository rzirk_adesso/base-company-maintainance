package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.CompanyDto;
import com.jobaidukraine.core.domain.Company;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "InRestCompanyMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {JobMapper.class, AddressMapper.class, UserMapper.class})
public interface CompanyMapper {
  Company dtoToDomain(CompanyDto companyDto);

  CompanyDto domainToDto(Company company);
}
