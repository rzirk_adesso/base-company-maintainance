# Contributing to the Job Aid Core Service

First off, thank you for taking the time to contribute! :+1: :tada:

## Table of Contents

- [Code of Conduct](#code-of-conduct)
- [How to Contribute](#how-to-contribute)
  - [Ask questions](#ask-questions)
  - [Create an Issue](#create-an-issue)
  - [Issue Lifecycle](#issue-lifecycle)
  - [Submit a Merge Request](#submit-a-merge-request)
  - [Participate in Reviews](#participate-in-reviews)
- [Build from Source](#build-from-source)
- [Source Code Style](#source-code-style)
- [Reference Docs](#reference-docs)

### Code of Conduct

This project is governed by the [JobAid Code of Conduct](https://gitlab.com/jobaid/base/-/tree/main/CODE_OF_CONDUCT.md).
By participating you are expected to uphold this code.
Please report unacceptable behavior to coc@jobaidukraine.com.

## How to Contribute

The core module follows the clean architecture [as described](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) by Robert C. Martin.

### Ask questions

If you have a question, feel free to join our [Slack channel](https://join.slack.com/share/enQtMzM5MDcxMTkxMTAyNi0wZTIyZDYzOWZjNTYyMDM0NzY0YzdiN2Q5NzA3OWZjMzA5ODdlYTBjODQ5ZjAwOWM4ZjViY2IwOTQ5ZTlkZGZj)!

If you believe there is an issue, search through
[existing issues](https://gitlab.com/jobaid/base/-/issues) trying a
few different ways to find discussions, past or current, that are related to the issue.
Reading those discussions helps you to learn about the issue, and helps us to make a
decision.

### Create an Issue

Reporting an issue or making a feature request is a great way to contribute. Your feedback
and the conversations that result from it provide a continuous flow of ideas. However,
before creating a ticket, please take the time to [ask and research](#ask-questions) first.

If you create an issue after a discussion on Slack, please provide a description.
The issue tracker is an important place of record for design discussions and should be
self-sufficient.

Once you're ready, create an issue on [GitLab](https://gitlab.com/jobaid/base/-/issues).

Many issues are caused by subtle behavior, typos, and unintended configuration.
Creating a [Minimal Reproducible Example](https://stackoverflow.com/help/minimal-reproducible-example)
of the problem helps the team quickly triage your issue and get to the core of the problem.

#### Issue Lifecycle

When an issue is first created, it is waiting for a team
member to triage it. Once the issue has been reviewed, the team may ask for further
information if needed, and based on the findings, the issue is either assigned a target
milestone or is closed with a specific status.

When a fix is ready, the issue is closed and may still be re-opened until the fix is
released. After that the issue will typically no longer be reopened. In rare cases if the
issue was not at all fixed, the issue may be re-opened. In most cases however any
follow-up reports will need to be created as new issues with a fresh description.

### Submit a Merge Request

1. Should you create an issue first? No, just create the merge request and use the
   description to provide context and motivation, as you would for an issue. If you want
   to start a discussion first or have already created an issue, once a merge request is
   created, we will close the issue as superseded by the merge request, and the discussion
   about the issue will continue under the merge request.

1. Always check out the `main` branch and submit merge requests against it
   (for target version see [build.gradle](https://gitlab.com/jobaid/base/-/blob/main/core/build.gradle)).

1. Choose the granularity of your commits consciously and squash commits that represent
   multiple edits or corrections of the same logical change. See
   [Rewriting History section of Pro Git](https://git-scm.com/book/en/Git-Tools-Rewriting-History)
   for an overview of streamlining the commit history.

1. Format commit messages using 55 characters for the subject line, 72 characters per line
   for the description, followed by the issue fixed, e.g. `Closes #12`. See the
   [Commit Guidelines section of Pro Git](https://git-scm.com/book/en/Distributed-Git-Contributing-to-a-Project#Commit-Guidelines)
   for best practices around commit messages, and use `git log` to see some examples.

1. If there is a prior issue, reference the GitLab issue number in the description of the
   merge request.

If accepted, your contribution may be heavily modified as needed prior to merging.
You will likely retain author attribution for your Git commits granted that the bulk of
your changes remain intact. You may also be asked to rework the submission.

If asked to make corrections, simply push the changes against the same branch, and your
merge request will be updated. In other words, you do not need to create a new merge request
when asked to make changes.

### Participate in Reviews

Helping to review merge requests is another great way to contribute. Your feedback can help to shape the implementation of new features.

## Build from Source

See [instructions](https://gitlab.com/jobaid/base/-/tree/main/core/documentation/build-from-source.md) on how to check out, build, and import the JobAid Core Service source code into your IDE.

## Source Code Style

Before submitting code make sure to follow our source code style guide which is following [Googles Style Guide](https://google.github.io/styleguide/javaguide.html).

By running `./gradlew spotlessApply` our code style can be applied automatically.
